# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "minitest/autorun"
require "mocha/mini_test"
require "rack/test"

require "sequel"
Sequel.extension :core_extensions
Sequel.extension :pg_range

require "lsr/web/exceptions"

# Prepend mock directory to load path.
$LOAD_PATH.insert(0, File.join(File.dirname(__FILE__), "..", "mocks"))
DB = :mock
require "lsr/web/app"

# Override with mock class
module LSR
  module Web
    ##
    # Mocks of the request handlers
    module Handlers
      def self.animal_update(_params)
        raise Sequel::DatabaseError
      end

      def self.animal_create(_params)
        raise Sequel::DatabaseError
      end

      def self.jquery_autocomplete_animal_query(_params)
        raise LSR::Web::InvalidGETParams
      end
    end
  end
end

class AppTest < Minitest::Test
  include Rack::Test::Methods

  def app
    LSR::Web::App
  end

  def test_create_error_caught
    LSR::Web::Handlers.expects(:on_edit_page_error)
    post "/animals/edit"
  end

  def test_create_error_responds_400
    post "/animals/edit", birthdate: "2016-11-02"
    assert_equal(400, last_response.status)
  end

  def test_update_errors_caught
    LSR::Web::Handlers.expects(:on_edit_page_error)
    post "/animals/edit", id: 42
  end

  def test_update_error_responds_400
    post "/animals/edit", id: 42, birthdate: "2016-11-02"
    assert_equal(400, last_response.status)
  end

  def test_jquery_autocomplete_animal_query_error_responds_400
    get "/jquery-autocomplete/animal"
    assert_equal(400, last_response.status)
  end
end
