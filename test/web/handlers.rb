# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "minitest/autorun"
require "mocha/mini_test"

require "sequel"
Sequel.extension :core_extensions
Sequel.extension :pg_range
DB = Mocha::Mock.new("db")

# Mock transaction method
def DB.transaction
  yield
end

require "lsr/web/exceptions"

# Prepend mock directory to load path.
$LOAD_PATH.insert(0, File.absolute_path(File.join(File.dirname(__FILE__), "..", "mocks")))
require "lsr/web/handlers/animal"
require "lsr/web/handlers/studs"

# Every key and value in the "params" Hash passed to the Handlers methods must
# be strings, as that is the type of every value received from an HTTP GET or
# POST request.
class HandlersAnimalBirthdateTest < Minitest::Test
  def test_animal_create_exact_birthdate
    LSR::DB::Animal.expects(:create)
                   .with(name: "Bob",
                         stud_id: nil,
                         sex: "male",
                         horned: "true",
                         breed: nil,
                         birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                         deathdate: nil,
                         father_id: nil,
                         mother_id: nil)
    LSR::Web::Handlers.animal_create("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02")
  end

  def test_animal_update_exact_birthdate
    mock_animal = mock
    mock_animal.expects(:update)
               .with(name: "Bob",
                     stud_id: nil,
                     sex: "male",
                     horned: "true",
                     breed: nil,
                     birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                     deathdate: nil,
                     father_id: nil,
                     mother_id: nil)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_update("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02")
  end

  def test_animal_update_range_birthdate
    mock_animal = mock
    mock_animal.expects(:update)
               .with(name: "Bob",
                     stud_id: nil,
                     sex: "male",
                     horned: "true",
                     breed: nil,
                     birthdate: (Date.new(2016, 11, 3)..Date.new(2016, 12, 6)).pg_range,
                     deathdate: nil,
                     father_id: nil,
                     mother_id: nil)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_update("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate_start_date" => "2016-11-03",
                                     "birthdate_end_date" => "2016-12-06")
  end

  def test_animal_update_invalid_birthdate_params
    mock_animal = mock
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    assert_raises(LSR::Web::POSTDataError,
                  "Did not raise expected exception when given birthdate start-date only") do
      LSR::Web::Handlers.animal_update("name" => "Bob",
                                       "horns" => "true",
                                       "sex" => "male",
                                       "birthdate_start_date" => "2016-11-03")
    end
    assert_raises(LSR::Web::POSTDataError,
                  "Did not raise expected exception when given birthdate end-date only") do
      LSR::Web::Handlers.animal_update("name" => "Bob",
                                       "horns" => "true",
                                       "sex" => "male",
                                       "birthdate_end_date" => "2016-11-06")
    end
  end
end

class HandlersAnimalDeathdateTest < Minitest::Test
  def test_animal_create_exact_deathdate
    LSR::DB::Animal.expects(:create)
                   .with(name: "Bob",
                         stud_id: nil,
                         sex: "male",
                         horned: "true",
                         breed: nil,
                         birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                         deathdate: (Date.new(2020, 2, 7)..Date.new(2020, 2, 7)).pg_range,
                         father_id: nil,
                         mother_id: nil)
    LSR::Web::Handlers.animal_create("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02",
                                     "deathdate" => "2020-02-07")
  end

  def test_animal_update_exact_deathdate
    mock_animal = mock
    mock_animal.expects(:update)
               .with(name: "Bob",
                     stud_id: nil,
                     sex: "male",
                     horned: "true",
                     breed: nil,
                     birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                     deathdate: (Date.new(2020, 2, 7)..Date.new(2020, 2, 7)).pg_range,
                     father_id: nil,
                     mother_id: nil)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_update("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02",
                                     "deathdate" => "2020-02-07")
  end

  def test_animal_update_range_deathdate
    mock_animal = mock
    mock_animal.expects(:update)
               .with(name: "Bob",
                     stud_id: nil,
                     sex: "male",
                     horned: "true",
                     breed: nil,
                     birthdate: (Date.new(2016, 11, 3)..Date.new(2016, 12, 6)).pg_range,
                     deathdate: (Date.new(2020, 2, 7)..Date.new(2020, 3, 10)).pg_range,
                     father_id: nil,
                     mother_id: nil)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_update("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate_start_date" => "2016-11-03",
                                     "birthdate_end_date" => "2016-12-06",
                                     "deathdate_start_date" => "2020-02-07",
                                     "deathdate_end_date" => "2020-03-10")
  end

  def test_animal_update_invalid_birthdate_params
    mock_animal = mock
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    assert_raises(LSR::Web::POSTDataError,
                  "Did not raise expected exception when given deathdate start-date only") do
      LSR::Web::Handlers.animal_update("name" => "Bob",
                                       "horns" => "true",
                                       "sex" => "male",
                                       "birthdate_start_date" => "2016-11-03",
                                       "birthdate_end_date" => "2016-12-06",
                                       "deathdate_start_date" => "2020-02-07")
    end
    assert_raises(LSR::Web::POSTDataError,
                  "Did not raise expected exception when given deathdate end-date only") do
      LSR::Web::Handlers.animal_update("name" => "Bob",
                                       "horns" => "true",
                                       "sex" => "male",
                                       "birthdate_start_date" => "2016-11-03",
                                       "birthdate_end_date" => "2016-12-06",
                                       "deathdate_end_date" => "2020-03-10")
    end
  end
end

class HandlersAnimalParentsTest < Minitest::Test
  def test_animal_create_with_parents
    mock_animal = mock("animal")
    LSR::DB::Animal.expects(:create)
                   .with(name: "Bob",
                         stud_id: nil,
                         sex: "male",
                         horned: "true",
                         breed: nil,
                         birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                         deathdate: nil,
                         father_id: 42,
                         mother_id: 24)
                   .returns(mock_animal)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_create("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02",
                                     "father" => "42",
                                     "mother" => "24")
  end

  def test_animal_update_with_parents
    mock_animal = mock("animal")
    mock_animal.expects(:update)
               .with(name: "Bob",
                     stud_id: nil,
                     sex: "male",
                     horned: "true",
                     breed: nil,
                     birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                     deathdate: nil,
                     father_id: 42,
                     mother_id: 24)
               .returns(mock_animal)
    LSR::DB::Animal.stubs(:[]).returns(mock_animal)
    LSR::Web::Handlers.animal_update("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "birthdate" => "2016-11-02",
                                     "father" => "42",
                                     "mother" => "24")
  end
end

class HandlersAnimalBreedTest < Minitest::Test
  def test_animal_nil_breed
    LSR::DB::Animal.expects(:create)
                   .with(name: "Bob",
                         stud_id: nil,
                         sex: "male",
                         horned: "true",
                         breed: nil,
                         birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                         deathdate: nil,
                         father_id: nil,
                         mother_id: nil)
    LSR::Web::Handlers.animal_create("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "breed" => "nil",
                                     "birthdate" => "2016-11-02")
  end

  def test_animal_non_nil_breed
    LSR::DB::Animal.expects(:create)
                   .with(name: "Bob",
                         stud_id: nil,
                         sex: "male",
                         horned: "true",
                         breed: "Toggenburg",
                         birthdate: (Date.new(2016, 11, 2)..Date.new(2016, 11, 2)).pg_range,
                         deathdate: nil,
                         father_id: nil,
                         mother_id: nil)
    LSR::Web::Handlers.animal_create("name" => "Bob",
                                     "sex" => "male",
                                     "horns" => "true",
                                     "breed" => "Toggenburg",
                                     "birthdate" => "2016-11-02")
  end
end

class HandlersAnimalParentAutocomplete < Minitest::Test
  def test_jquery_autocomplete_animal
    search_term = "Aa"

    LSR::DB::Animal.expects(:where)
                   .with(Sequel.ilike(:name, "#{search_term}%"),
                         sex: "male")
    LSR::Web::Handlers.jquery_autocomplete_animal_query("term" => search_term,
                                                        "sex" => "male")
  end

  def test_jquery_autocomplete_no_term
    assert_raises(LSR::Web::InvalidGETParams,
                  "Did not raise expected exception for missing search term") do
      LSR::Web::Handlers.jquery_autocomplete_animal_query("sex" => "male")
    end
  end

  def test_jquery_autocomplete_no_sex
    assert_raises(LSR::Web::InvalidGETParams,
                  "Did not raise expected exception for missing sex") do
      LSR::Web::Handlers.jquery_autocomplete_animal_query("term" => "Aa")
    end
  end

  def test_jquery_autocomplete_invalid_sex
    assert_raises(LSR::Web::InvalidGETParams,
                  "Did not raise expected exception for invalid sex") do
      LSR::Web::Handlers.jquery_autocomplete_animal_query("sex" => "INVALID")
    end
  end
end

class HandlersStudsTest < Minitest::Test
  def test_studs_update_only_existing
    mock_stud1 = mock
    mock_stud2 = mock
    LSR::DB::Stud.stubs(:[]).with { |*args| args[0] == {id: 1} }.returns mock_stud1
    LSR::DB::Stud.stubs(:[]).with { |*args| args[0] == {id: 2} }.returns mock_stud2
    mock_stud1.expects(:update).with(name: "Red Farm")
    mock_stud2.expects(:update).with(name: "Blue Farm")

    LSR::Web::Handlers.studs_update("id1" => "Red Farm",
                                    "id2" => "Blue Farm")
  end

  def test_studs_update_only_new
    LSR::DB::Stud.expects(:create).with(name: "Red Farm")
    LSR::DB::Stud.expects(:create).with(name: "Blue Farm")

    LSR::Web::Handlers.studs_update("new1" => "Red Farm",
                                    "new2" => "Blue Farm")
  end

  def test_studs_update_existing_and_new
    mock_stud1 = mock
    mock_stud2 = mock
    LSR::DB::Stud.stubs(:[]).with { |*args| args[0] == {id: 1} }.returns mock_stud1
    LSR::DB::Stud.stubs(:[]).with { |*args| args[0] == {id: 2} }.returns mock_stud2
    mock_stud1.expects(:update).with(name: "Red Farm")
    mock_stud2.expects(:update).with(name: "Blue Farm")

    LSR::DB::Stud.expects(:create).with(name: "Orange Farm")
    LSR::DB::Stud.expects(:create).with(name: "Cyan Farm")

    LSR::Web::Handlers.studs_update("id1" => "Red Farm",
                                    "id2" => "Blue Farm",
                                    "new1" => "Orange Farm",
                                    "new2" => "Cyan Farm")
  end
end
