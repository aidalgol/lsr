#!/usr/bin/env ruby
# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# In this test file, we disable the RuboCop Metrics/LineLength on lines with
# long strings that would be made less readable if broken up across multiple
# lines to satisfy the line-length requirement.

require "sequel"
Sequel.extension :core_extensions
Sequel.extension :pg_range
DB = Sequel.connect(ENV.fetch("DATABASE_URL"))

require "minitest/autorun"
require "lsr/db"
require "date"

class SequelTestCase < Minitest::Test
  def run(*args, &block)
    result = nil
    Sequel::Model.db.transaction(rollback: :always, auto_savepoint: true) do
      result = super
    end
    result
  end
end

class AnimalTestBase < SequelTestCase
  INCORRECT_EXCEPTION_MESSAGE = "Incorrect exception-message"

  DATE_RANGE_EARLIER = Date.new(2016, 4)...Date.new(2016, 5)
  DATE_RANGE_LATER = Date.new(2017, 7)...Date.new(2017, 8)

  private_constant :INCORRECT_EXCEPTION_MESSAGE,
                   :DATE_RANGE_EARLIER,
                   :DATE_RANGE_LATER

  def opposite_sex(sex)
    {male: "female",
     female: "male"}[sex.to_sym]
  end
  private :opposite_sex

  def values_for_parent(parent)
    case parent
    when :father
      ["male", :father_id]
    when :mother
      ["female", :mother_id]
    end
  end
  private :values_for_parent
end

class AnimalTestBirthdate < AnimalTestBase
  def test_birthdate_valid_date
    ex = assert_raises(Sequel::DatabaseError, "Did not raise expected exception when given non-range date for birthdate") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: Date.new(2016, 4, 1))
    end
    assert_match(/^PG::InvalidTextRepresentation: ERROR:  malformed range literal:/,
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)

    assert_equal(DATE_RANGE_EARLIER,
                 LSR::DB::Animal.create(
                   sex: "male",
                   birthdate: DATE_RANGE_EARLIER.pg_range).values[:birthdate].to_range)
  end

  def test_birthdate_empty_daterange
    ex = assert_raises(Sequel::CheckConstraintViolation,
                       "Did not raise expected exception when given empty daterange for birthdate") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2016, 4, 9)...Date.new(2016, 4, 9)).pg_range)
    end
    assert_match(/^PG::CheckViolation: ERROR:  new row for relation "animals" violates check constraint "birthdate_nonempty_range"\nDETAIL:/, # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end

  def test_birthdate_exact_day
    assert_equal(Date.new(2016, 7, 23)...Date.new(2016, 7, 24),
                 LSR::DB::Animal.create(sex: "male",
                                        birthdate: (Date.new(2016, 7, 23)..Date.new(2016, 7, 23))
                                          .pg_range).values[:birthdate].to_range)
  end
end

class AnimalTestParents < AnimalTestBase
  def test_destroy_with_no_parents
    animal = LSR::DB::Animal.create(name: "Bob",
                                    sex: "male",
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    animal.destroy
    assert_equal(LSR::DB::Animal[id: animal.id], nil)
  end

  def destroy_parent_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: parent_sex, birthdate: DATE_RANGE_EARLIER.pg_range)
    LSR::DB::Animal.create(sex: "male",
                           parent_column => parent.id,
                           birthdate: DATE_RANGE_LATER.pg_range)

    ex = assert_raises(Sequel::ForeignKeyConstraintViolation,
                       "Incorrect response for deleting an animal that is the #{which_parent} of another animal") do # rubocop:disable Metrics/LineLength
      parent.destroy
    end
    assert_match(/\APG::ForeignKeyViolation: ERROR:.*DETAIL:  Key \(id\)=\(\p{Digit}+\) is still referenced from table "animals"\.\Z/m, # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :destroy_parent_test

  def test_destroy_father
    destroy_parent_test(:father)
  end

  def test_destroy_mother
    destroy_parent_test(:mother)
  end

  def destroy_child_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: parent_sex, birthdate: DATE_RANGE_EARLIER.pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   parent_column => parent.id,
                                   birthdate: DATE_RANGE_LATER.pg_range)
    child.destroy
    assert_equal(LSR::DB::Animal[id: child.id], nil, "Animal still in dataset after deleting")
  end
  private :destroy_child_test

  def test_destroy_child_with_father
    destroy_child_test(:father)
  end

  def test_destroy_child_with_mother
    destroy_child_test(:mother)
  end

  # We test that an animal can be created with parent ONLY in the most basic
  # testcases so that this is covered, and this is deliberately left out of the
  # trigger testcases to minimise doubling up of coverage.
  def parent_foreign_key_constraint_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    assert_raises(Sequel::ForeignKeyConstraintViolation,
                  "Incorrect response for non-existent foreign-key") do
      LSR::DB::Animal.create(sex: "male",
                             birthdate: DATE_RANGE_LATER.pg_range,
                             parent_column => 9001)
    end
    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    assert_equal(parent.id,
                 LSR::DB::Animal.create(sex: "male",
                                        birthdate: DATE_RANGE_LATER.pg_range,
                                        parent_column => parent.id).values[parent_column])
  end
  private :parent_foreign_key_constraint_test

  def test_father_foreign_key_constraint
    parent_foreign_key_constraint_test(:father)
  end

  def test_mother_foreign_key_constraint
    parent_foreign_key_constraint_test(:mother)
  end

  def child_cant_be_its_own_parent_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    animal = LSR::DB::Animal.create(sex: parent_sex, birthdate: DATE_RANGE_LATER.pg_range)
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for animal as its own #{which_parent}") do
      animal[parent_column] = animal.id
      animal.save
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  An animal can not be its own #{which_parent}\n", # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :child_cant_be_its_own_parent_test

  def test_child_cant_be_its_own_father
    child_cant_be_its_own_parent_test(:father)
  end

  def test_child_cant_be_its_own_mother
    child_cant_be_its_own_parent_test(:mother)
  end
end

class AnimalTestBirthdate < AnimalTestBase
  def birthdate_trigger_on_child_create_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} younger than child\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 5)...Date.new(2016, 8)).pg_range)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with younger #{which_parent}") do
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2015, 5)...Date.new(2015, 6)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with birthdate overlapping #{which_parent}'s on the earlier side") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2016, 4)...Date.new(2016, 6)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with birthdate overlapping #{which_parent}'s on the later side") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2016, 7)...Date.new(2016, 9)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :birthdate_trigger_on_child_create_test

  def test_birthdate_trigger_on_child_create_father
    birthdate_trigger_on_child_create_test(:father)
  end

  def test_birthdate_trigger_on_child_create_mother
    birthdate_trigger_on_child_create_test(:mother)
  end

  def birthdate_trigger_on_child_update_parent_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} younger than child\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2017, 7)...Date.new(2017, 8)).pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   birthdate: (Date.new(2016, 7)...Date.new(2016, 10)).pg_range)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} that is younger than the child") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    parent.birthdate = (Date.new(2016, 5)...Date.new(2016, 8)).pg_range
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} that overlaps the child's birthdate on the earlier side") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    parent.birthdate = (Date.new(2016, 9)...Date.new(2016, 12)).pg_range
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} that overlaps the child's birthdate on the later side") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :birthdate_trigger_on_child_update_parent_test

  def test_birthdate_trigger_on_child_update_father
    birthdate_trigger_on_child_update_parent_test(:father)
  end

  def test_birthdate_trigger_on_child_update_mother
    birthdate_trigger_on_child_update_parent_test(:mother)
  end

  def birthdate_trigger_on_child_update_birthdate_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} younger than child\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 7)...Date.new(2016, 10)).pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   birthdate: (Date.new(2017, 8)...Date.new(2017, 9)).pg_range,
                                   parent_column => parent.id)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to earlier than its #{which_parent}") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2015, 7)...Date.new(2015, 8)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to overlap with its #{which_parent}'s on the earlier side") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2016, 5)...Date.new(2016, 8)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to overlap with its #{which_parent}'s on the later side") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2016, 9)...Date.new(2016, 10)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :birthdate_trigger_on_child_update_birthdate_test

  def test_birthdate_trigger_on_child_update_birthdate_father
    birthdate_trigger_on_child_update_birthdate_test(:father)
  end

  def test_birthdate_trigger_on_child_update_birthdate_mother
    birthdate_trigger_on_child_update_birthdate_test(:mother)
  end

  def parent_birthdate_change_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  Cannot change a parent to be younger than any of its children\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 8)...Date.new(2016, 9)).pg_range)
    # Create multiple children of the parent, with the oldest NOT at the start
    # of the list, in order to cover the loop in the trigger we're testing.
    LSR::DB::Animal.create(sex: "female",
                           birthdate: (Date.new(2019, 7)...Date.new(2019, 10)).pg_range,
                           parent_column => parent.id)
    LSR::DB::Animal.create(sex: "male",
                           birthdate: (Date.new(2017, 8)...Date.new(2017, 11)).pg_range,
                           parent_column => parent.id)
    LSR::DB::Animal.create(sex: "male",
                           birthdate: (Date.new(2020, 9)...Date.new(2020, 12)).pg_range,
                           parent_column => parent.id)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s birthdate to be earlier than (at least one of) its children") do # rubocop:disable Metrics/LineLength
      # Set the parent to be younger only than the oldest child.
      parent.birthdate = (Date.new(2018, 11)...Date.new(2018, 12)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s birthdate to overlap (at least one of) its children on the earlier side") do # rubocop:disable Metrics/LineLength
      # Set the parent's birthdate to overlap only the oldest child.
      parent.birthdate = (Date.new(2017, 7)...Date.new(2017, 9)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s birthdate to overlap (at least one of) its children on the later side") do # rubocop:disable Metrics/LineLength
      # Set the parent's birthdate to overlap only the oldest child.
      parent.birthdate = (Date.new(2017, 10)...Date.new(2017, 12)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :parent_birthdate_change_test

  def test_father_birthdate_change
    parent_birthdate_change_test(:father)
  end

  def test_mother_birthdate_change
    parent_birthdate_change_test(:mother)
  end
end

class AnimalTestDeathdate < AnimalTestBase
  def deathdate_trigger_on_child_create_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} died before child birthdate\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 5)...Date.new(2016, 8)).pg_range,
                                    deathdate: (Date.new(2021, 2)...Date.new(2021, 7)).pg_range)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with dead #{which_parent}") do
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2022, 5)...Date.new(2022, 6)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with birthdate overlapping #{which_parent}'s deathdate on the earlier side") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2020, 10)...Date.new(2021, 3)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for creating child with birthdate overlapping #{which_parent}'s deathdate on the later side") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2021, 6)...Date.new(2021, 10)).pg_range,
                             parent_column => parent.id)
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :deathdate_trigger_on_child_create_test

  def test_deathdate_trigger_on_child_create_father
    deathdate_trigger_on_child_create_test(:father)
  end

  def test_deathdate_trigger_on_child_create_mother
    deathdate_trigger_on_child_create_test(:mother)
  end

  def deathdate_trigger_on_child_update_parent_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} died before child birthdate\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 5)...Date.new(2016, 8)).pg_range,
                                    deathdate: (Date.new(2021, 2)...Date.new(2021, 7)).pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   birthdate: (Date.new(2021, 10)...Date.new(2022, 2)).pg_range)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} that died before than the child's birthdate") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    parent.deathdate = (Date.new(2021, 5)...Date.new(2021, 11)).pg_range
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} whose deathdate overlaps the child's birthdate on the earlier side") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    parent.birthdate = (Date.new(2022, 1)...Date.new(2012, 5)).pg_range
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting a #{which_parent} whose deathdate overlaps the child's birthdate on the later side") do # rubocop:disable Metrics/LineLength
      child[parent_column] = parent.id
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :deathdate_trigger_on_child_update_parent_test

  def test_deathdate_trigger_on_child_update_father
    deathdate_trigger_on_child_update_parent_test(:father)
  end

  def test_deathdate_trigger_on_child_update_mother
    deathdate_trigger_on_child_update_parent_test(:mother)
  end

  def deathdate_trigger_on_child_update_birthdate_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  #{which_parent.capitalize} died before child birthdate\n" # rubocop:disable Metrics/LineLength

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: (Date.new(2016, 5)...Date.new(2016, 8)).pg_range,
                                    deathdate: (Date.new(2021, 2)...Date.new(2021, 7)).pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   birthdate: (Date.new(2017, 8)...Date.new(2017, 9)).pg_range,
                                   parent_column => parent.id)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to later than its #{which_parent}'s deathdate") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2021, 8)...Date.new(2021, 9)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to overlap with its #{which_parent}'s deathdate on the earlier side") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2021, 1)...Date.new(2021, 3)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting child's birthdate to overlap with its #{which_parent}'s deathdate on the later side") do # rubocop:disable Metrics/LineLength
      child.birthdate = (Date.new(2021, 6)...Date.new(2021, 8)).pg_range
      child.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :deathdate_trigger_on_child_update_birthdate_test

  def test_deathdate_trigger_on_child_update_deathdate_father
    deathdate_trigger_on_child_update_birthdate_test(:father)
  end

  def test_deathdate_trigger_on_child_update_deathdate_mother
    deathdate_trigger_on_child_update_birthdate_test(:mother)
  end

  def parent_deathdate_change_test(which_parent, parent)
    _, parent_column = values_for_parent(which_parent)
    postgres_exception_message = "PG::IntegrityConstraintViolation: ERROR:  Cannot change a parent to have died before than any of its childrens birthdates\n" # rubocop:disable Metrics/LineLength

    # Create multiple children of the parent, with the youngest NOT at the start
    # of the list, in order to cover the loop in the trigger we're testing.
    LSR::DB::Animal.create(sex: "female",
                           birthdate: (Date.new(2019, 7)...Date.new(2019, 10)).pg_range,
                           parent_column => parent.id)
    LSR::DB::Animal.create(sex: "male",
                           birthdate: (Date.new(2017, 8)...Date.new(2017, 11)).pg_range,
                           parent_column => parent.id)
    LSR::DB::Animal.create(sex: "male",
                           birthdate: (Date.new(2020, 9)...Date.new(2020, 12)).pg_range,
                           parent_column => parent.id)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s deathdate to be earlier than (at least one of) its children's birthdates") do # rubocop:disable Metrics/LineLength
      # Set the parent to be younger only than the oldest child.
      parent.deathdate = (Date.new(2020, 7)...Date.new(2020, 8)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s deathdate to overlap (at least one of) its children's birthdates on the earlier side") do # rubocop:disable Metrics/LineLength
      # Set the parent's birthdate to overlap only the oldest child.
      parent.birthdate = (Date.new(2020, 8)...Date.new(2020, 10)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)

    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing #{which_parent}'s deathdate to overlap (at least one of) its children's birthdates on the later side") do # rubocop:disable Metrics/LineLength
      # Set the parent's birthdate to overlap only the oldest child.
      parent.birthdate = (Date.new(2020, 11)...Date.new(2021, 1)).pg_range
      parent.save
    end
    assert_equal(postgres_exception_message, ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :parent_deathdate_change_test

  def test_father_deathdate_set
    parent_deathdate_change_test(
      :father,
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2016, 8)...Date.new(2016, 9)).pg_range))
  end

  def test_mother_deathdate_set
    parent_deathdate_change_test(
      :mother,
      LSR::DB::Animal.create(sex: "female",
                             birthdate: (Date.new(2016, 8)...Date.new(2016, 9)).pg_range))
  end

  def test_father_deathdate_change
    parent_deathdate_change_test(
      :father,
      LSR::DB::Animal.create(sex: "male",
                             birthdate: (Date.new(2016, 8)...Date.new(2016, 9)).pg_range,
                             deathdate: (Date.new(2022, 6)...Date.new(2022, 7)).pg_range))
  end

  def test_mother_deathdate_change
    parent_deathdate_change_test(
      :mother,
      LSR::DB::Animal.create(sex: "female",
                             birthdate: (Date.new(2016, 8)...Date.new(2016, 9)).pg_range,
                             deathdate: (Date.new(2022, 6)...Date.new(2022, 7)).pg_range))
  end
end

class AnimalTestSex < AnimalTestBase
  def parent_sex_trigger_on_create_child_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: opposite_sex(parent_sex),
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    ex = assert_raises(
      Sequel::DatabaseError,
      "Incorrect response for new animal with #{opposite_sex(parent_sex)} #{which_parent}") do
      LSR::DB::Animal.create(sex: "male",
                             birthdate: DATE_RANGE_LATER.pg_range,
                             parent_column => parent.id)
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  Non-#{parent_sex} #{which_parent}\n",
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :parent_sex_trigger_on_create_child_test

  def test_father_sex_trigger_on_create_child
    parent_sex_trigger_on_create_child_test(:father)
  end

  def test_mother_sex_trigger_on_create_child
    parent_sex_trigger_on_create_child_test(:mother)
  end

  def parent_sex_trigger_on_change_child_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    _valid_parent  = LSR::DB::Animal.create(sex: parent_sex,
                                            birthdate: DATE_RANGE_EARLIER.pg_range)
    invalid_parent = LSR::DB::Animal.create(sex: opposite_sex(parent_sex),
                                            birthdate: DATE_RANGE_EARLIER.pg_range)
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing an animal #{which_parent} to point to a #{opposite_sex(parent_sex)}") do # rubocop:disable Metrics/LineLength
      LSR::DB::Animal.create(sex: "male",
                             birthdate: DATE_RANGE_LATER.pg_range,
                             parent_column => invalid_parent.id)
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  Non-#{parent_sex} #{which_parent}\n",
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :parent_sex_trigger_on_change_child_test

  def test_father_sex_trigger_on_change_child
    parent_sex_trigger_on_change_child_test(:father)
  end

  def test_mother_sex_trigger_on_change_child
    parent_sex_trigger_on_change_child_test(:mother)
  end

  def parent_sex_change_trigger_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    _child = LSR::DB::Animal.create(sex: "male",
                                    birthdate: DATE_RANGE_LATER.pg_range,
                                    parent_column => parent.id)
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for changing sex of a parent") do
      parent.sex = opposite_sex(parent_sex)
      parent.save
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  Not allowed to change sex of an animal with children\n", # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :parent_sex_change_trigger_test

  def test_father_sex_change_trigger
    parent_sex_change_trigger_test(:father)
  end

  def test_mother_sex_change_trigger
    parent_sex_change_trigger_test(:mother)
  end
end

class AnimalTestBreed < AnimalTestBase
  def breed_trigger_on_create_child_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting breed on a child") do
      _child = LSR::DB::Animal.create(sex: "male",
                                      breed: "Toggenburg",
                                      birthdate: DATE_RANGE_LATER.pg_range,
                                      parent_column => parent.id)
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  Can specify breed only for top-level ancestors\n", # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :breed_trigger_on_create_child_test

  def test_breed_trigger_on_create_child_with_father
    breed_trigger_on_create_child_test(:father)
  end

  def test_breed_trigger_on_create_child_with_mother
    breed_trigger_on_create_child_test(:mother)
  end

  def breed_trigger_on_change_child_test(which_parent)
    parent_sex, parent_column = values_for_parent(which_parent)

    parent = LSR::DB::Animal.create(sex: parent_sex,
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    child = LSR::DB::Animal.create(sex: "male",
                                   birthdate: DATE_RANGE_LATER.pg_range,
                                   parent_column => parent.id)
    ex = assert_raises(Sequel::DatabaseError,
                       "Incorrect response for setting breed on a child") do
      child.breed = "Toggenburg"
      child.save
    end
    assert_equal("PG::IntegrityConstraintViolation: ERROR:  Can specify breed only for top-level ancestors\n", # rubocop:disable Metrics/LineLength
                 ex.message, INCORRECT_EXCEPTION_MESSAGE)
  end
  private :breed_trigger_on_change_child_test

  def test_breed_trigger_on_change_child_with_father
    breed_trigger_on_create_child_test(:father)
  end

  def test_breed_trigger_on_change_child_with_mother
    breed_trigger_on_create_child_test(:mother)
  end

  def test_breed_trigger_on_no_parent_create
    assert_equal("Toggenburg",
                 LSR::DB::Animal.create(sex: "male",
                                        breed: "Toggenburg",
                                        birthdate: DATE_RANGE_EARLIER.pg_range).values[:breed])
  end

  def test_breed_trigger_on_no_parent_update
    animal = LSR::DB::Animal.create(sex: "male",
                                    birthdate: DATE_RANGE_EARLIER.pg_range)
    animal.breed = "Toggenburg"
    animal.save
    assert_equal("Toggenburg", animal.breed)
  end
end
