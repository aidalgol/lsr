# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "minitest/autorun"
require "lsr/coercion"

class CoercionTest < Minitest::Test
  def test_string_to_integer_valid_integer_string
    assert_equal(42, LSR::Coercion.string_to_integer_or_nil("42"))
    assert_equal(0, LSR::Coercion.string_to_integer_or_nil("0"))
    assert_equal(-42, LSR::Coercion.string_to_integer_or_nil("-42"))
  end

  def test_string_to_integer_invalid_integer_string
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil("a42")
    end
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil("42a")
    end
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil("a42")
    end
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil("abc")
    end
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil("42".to_sym)
    end
  end

  def test_string_to_integer_invalid_type
    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil(nil)
    end

    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil(42)
    end

    assert_raises(ArgumentError) do
      LSR::Coercion.string_to_integer_or_nil(42.7)
    end
  end

  def test_nil_or_string
    assert_equal("foo", LSR::Coercion.nil_or_string("foo"))
    assert_equal(nil, LSR::Coercion.nil_or_string("nil"))
    assert_equal(nil, LSR::Coercion.nil_or_string(nil))
  end
end
