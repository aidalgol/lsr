# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "minitest/autorun"
require "lsr/range_extensions"

class RangeTest < Minitest::Test
  def test_date_single_value?
    assert((Date.new(2016, 4, 19)..Date.new(2016, 4, 19)).single_value?)
    refute((Date.new(2016, 4, 19)...Date.new(2016, 4, 19)).single_value?)
    assert((Date.new(2016, 4, 19)...Date.new(2016, 4, 20)).single_value?)
    refute((Date.new(2016, 5, 20)..Date.new(2016, 5, 23)).single_value?)
  end
end
