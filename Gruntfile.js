/* This file is part of LSR, a software package for managing LiveStock Records
 * Copyright (C) 2016  Aidan Gauland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* global module */ // Tell ESLint that `module` is defined.
module.exports = function(grunt) {
  "use strict"

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),

    // Lint
    eslint: {
      all: ["Gruntfile.js", "lib/**/*.js"]
    },
  })

  // Load plugins
  grunt.loadNpmTasks("grunt-eslint")

  // Task aliases
  grunt.registerTask("default", "eslint")
}
