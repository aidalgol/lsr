=================
LiveStock Records
=================
LSR is a software package for managing livestock records, using the Ruby Sequel_ ORM.


Spinning up the Docker Compose services
-----------------------------------------
LSR uses Docker Compose for consistency across development and deployment environments.  During development, you will 

In its own terminal, run

::

   docker-compose up

Now when running any of the Rake tasks that connect to a database, set the environment variable `DATABASE_URL` to `postgres://postgres@localhost:9001/postgres`.

Before actually trying to use the web UI, you need to run the migrations::

   DATABASE_URL="postgres://postgres@localhost:9001/postgres" rake db:migrate

and then restart the web service so that the Sequel models are reloaded to match the migrated database schema::

  docker-compose restart web


Tests
-----
The database tests (Rake task `db:tests`) need a database to connect to (see above).  The web-UI tests (Rake task `web:tests`) do not.

.. _Sequel: http://sequel.jeremyevans.net/
.. _PostgreSQL: https://www.postgresql.org/
.. _Ruby: http://www.ruby-lang.org/
.. _Minitest: https://github.com/seattlerb/minitest
.. _Erector: http://erector.github.io/erector/
