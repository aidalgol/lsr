# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "rake/testtask"
require "rubocop/rake_task"

namespace "db" do # rubocop:disable Metrics/BlockLength
  # Library dependencies common to all tasks in this namespace.
  require "sequel"

  desc "Run the Sequel migrations"
  task :migrate, [:version] do |_, args|
    # This task is based on the example from the Sequel documentation.
    Sequel.extension :migration
    Sequel.connect(ENV.fetch("DATABASE_URL")) do |db|
      if args[:version]
        puts "Migrating to version #{args[:version]}"
        Sequel::Migrator.run(db, "migrations", target: args[:version].to_i)
      else
        puts "Migrating to latest"
        Sequel::Migrator.run(db, "migrations")
      end
    end
  end

  Rake::TestTask.new(:tests => :migrate) do |t|
    t.description = "Run tests for Sequel model"
    # Add the directory of this Rakefile to the tests load-path.
    t.libs << File.dirname(__FILE__)
    t.test_files = ["test/db.rb"]
  end

  desc "Run an IRB or Pry session connected to a database"
  task :repl, [:name] => :migrate do |_, args|
    Sequel.connect(ENV.fetch("DATABASE_URL")) do |db|
      error "REPL name must be specified" if args.name.nil?
      Sequel.extension :core_extensions
      Sequel.extension :pg_range
      DB = db
      $LOAD_PATH.insert(0, File.join(File.dirname(__FILE__), "lib"))
      require "lsr/db"
      ARGV.clear
      case args.name.strip
      when "irb"
        require "irb"
        IRB.start
      when "pry"
        require "pry"
        Pry.start
      end
    end
  end
end

namespace "web" do
  desc "Run the web UI server"
  task :serve => "db:migrate" do
    exec "rackup", "-I", "lib", "-p", "9292"
  end

  desc "Run tests for the web UI"
  task :tests do
    # Add the directory of this Rakefile to the tests load-path.
    libs_dir = File.join(File.dirname(__FILE__), "lib")
    FileList["test/web/handlers.rb", "test/web/app.rb"].each do |filename|
      puts "Running test file #{filename}"
      ruby "-I#{libs_dir}", filename
    end
  end
end

Rake::TestTask.new(:misc_tests) do |t|
  t.description = "Run the miscellaneous tests"
  # Add the directory of this Rakefile to the tests load-path.
  t.libs << File.dirname(__FILE__)
  t.test_files = ["test/range_extensions.rb", "test/coercion.rb"]
end

RuboCop::RakeTask.new
