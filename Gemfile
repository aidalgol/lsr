# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ruby "~> 2.3.1"

source "https://rubygems.org"

# Database libraries
gem "sequel", "~> 4.40.0", group: :db
gem "sequel_pg", "~> 1.6.17", group: :db

# Web libraries
gem "sinatra", "~> 1.4.7", group: :web
gem "sinatra-contrib", "~> 1.4.7", group: :web
gem "sprockets", "~> 3.7.0", group: :web
gem "erector", "~> 0.10.0", group: :web
gem "activesupport", "~> 5.0.0.1", group: :web

# Test frameworks
gem "minitest", "~> 5.9.1", group: :testing
gem "mocha", "~> 1.2.1", group: :testing
gem "rack-test", "~> 0.6.3", group: :testing

# Development tools
gem "rubocop", "~> 0.46.0", groups: [:testing, :development]
gem "rake", "~> 11.3.0", groups: [:testing, :development]
gem "pry", "~> 0.10.4", group: :development
