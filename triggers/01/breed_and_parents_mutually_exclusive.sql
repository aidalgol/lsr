-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION breed_and_parents_mutually_exclusive() RETURNS trigger AS $breed_and_parents_mutually_exclusive$
    DECLARE
        trace INTEGER;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- Raise an exception if the breed column is non-NULL and either parent
        -- column is non-NULL.
        IF (NEW.breed IS NOT NULL) AND
           ((NEW.father_id IS NOT NULL) OR (NEW.mother_id IS NOT NULL)) THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Can specify breed only for top-level ancestors';
        END IF;

        -- Return NEW for all non-DELETE operations.
        RETURN NEW;
    END;
$breed_and_parents_mutually_exclusive$ LANGUAGE plpgsql;
