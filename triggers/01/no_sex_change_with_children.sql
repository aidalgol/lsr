-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION no_sex_change_with_children() RETURNS trigger AS $no_sex_change_with_children$
    DECLARE
        trace INTEGER;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- If the sex of this animal changed...
        IF TG_OP = 'UPDATE' AND NEW.sex <> OLD.sex THEN
            -- find out how many children it has.
            SELECT id FROM animals
            WHERE father_id = NEW.id or
                  mother_id = NEW.id
            INTO trace;
            -- Raise an exception if it has any.
            IF trace IS NOT NULL THEN
                RAISE integrity_constraint_violation USING MESSAGE = 'Not allowed to change sex of an animal with children';
            END IF;
        END IF;

        -- Return NEW for all non-DELETE operations.
        RETURN NEW;
    END;
$no_sex_change_with_children$ LANGUAGE plpgsql;
