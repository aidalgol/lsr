-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION not_own_parent() RETURNS trigger AS $not_own_parent$
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- Neither parent ID can be this (their child's) ID, because that would
        -- mean the animal is its own parent.
        IF NEW.id = NEW.father_id THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'An animal can not be its own father';
        END IF;
        IF NEW.id = NEW.mother_id THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'An animal can not be its own mother';
        END IF;

        RETURN NEW;
    END;
$not_own_parent$ LANGUAGE plpgsql;
