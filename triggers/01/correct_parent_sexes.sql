-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION correct_parent_sexes() RETURNS trigger AS $correct_parent_sexes$
    DECLARE
        parent_sex SEX;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- Each parent must be of the appropriate sex for that role.  That
        -- is, the mother must be female, and the father must be male.
        SELECT sex FROM animals WHERE id = NEW.father_id INTO parent_sex;
        IF parent_sex <> 'male' THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Non-male father';
        END IF;
        SELECT sex FROM animals WHERE id = NEW.mother_id INTO parent_sex;
        IF parent_sex <> 'female' THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Non-female mother';
        END IF;

        RETURN NEW;
    END;
$correct_parent_sexes$ LANGUAGE plpgsql;
