-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION parents_born_before_child() RETURNS trigger AS $parents_born_before_child$
    DECLARE
        parent_birthdate DATERANGE;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- Both parents must have a birthdate earlier than that of the child.
        -- As the birthdate column is a range, the ranges must NOT overlap.
        SELECT birthdate FROM animals WHERE id = NEW.father_id INTO parent_birthdate;
        IF NOT (parent_birthdate << NEW.birthdate) THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Father younger than child';
        END IF;
        SELECT birthdate FROM animals WHERE id = NEW.mother_id INTO parent_birthdate;
        IF NOT (parent_birthdate << NEW.birthdate) THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Mother younger than child';
        END IF;

        RETURN NEW;
    END;
$parents_born_before_child$ LANGUAGE plpgsql;
