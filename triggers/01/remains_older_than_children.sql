-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION remains_older_than_children() RETURNS trigger AS $remains_older_than_children$
    DECLARE
        child_birthdate DATERANGE;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- If the birthdate of this animal changed...
        IF TG_OP = 'UPDATE' AND NEW.birthdate <> OLD.birthdate THEN
            -- Raise an exception if the new birthdate range is not strictly
            -- earlier than all of its children.
            FOR child_birthdate IN
                SELECT birthdate FROM animals
                WHERE father_id = NEW.id or
                      mother_id = NEW.id
            LOOP
                IF NOT (child_birthdate >> NEW.birthdate) THEN
                    RAISE integrity_constraint_violation USING MESSAGE = 'Cannot change a parent to be younger than any of its children';
                END IF;
            END LOOP;
        END IF;

        -- Return NEW for all non-DELETE operations.
        RETURN NEW;
    END;
$remains_older_than_children$ LANGUAGE plpgsql;
