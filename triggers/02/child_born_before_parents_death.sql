-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION child_born_before_parents_death() RETURNS trigger AS $child_born_before_parents_death$
    DECLARE
        parent_deathdate DATERANGE;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- Neither parent can have a deathdate before that of the child's
        -- birthdate.  Since the columns are ranges, the parent death and child
        -- birth ranges must NOT overlap.
        SELECT deathdate FROM animals WHERE id = NEW.father_id INTO parent_deathdate;
        IF NOT (NEW.birthdate << parent_deathdate) THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Father died before child birthdate';
        END IF;
        SELECT deathdate FROM animals WHERE id = NEW.mother_id INTO parent_deathdate;
        IF NOT (NEW.birthdate << parent_deathdate) THEN
            RAISE integrity_constraint_violation USING MESSAGE = 'Mother died before child birthdate';
        END IF;
        RETURN NEW;
    END;
$child_born_before_parents_death$ LANGUAGE plpgsql;
