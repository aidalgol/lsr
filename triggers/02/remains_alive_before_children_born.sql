-- This file is part of LSR, a software package for managing LiveStock Records
-- Copyright (C) 2016  Aidan and Michael Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

CREATE OR REPLACE FUNCTION remains_alive_before_children_born() RETURNS trigger AS $remains_alive_before_children_born$
    DECLARE
        child_birthdate DATERANGE;
    BEGIN
        -- Just return OLD immediately for DELETEs.
        IF TG_OP = 'DELETE' THEN
           RETURN OLD;
        END IF;

        -- If the deathdate of this animal changed...
        IF TG_OP = 'UPDATE' AND NEW.deathdate IS NOT NULL AND
           (OLD.deathdate IS NULL OR NEW.deathdate <> OLD.deathdate) THEN
            -- Raise an exception if the new deathdate range is not strictly
            -- later than all of its children's birthdates.
            FOR child_birthdate IN
                SELECT birthdate FROM animals
                WHERE father_id = NEW.id or
                      mother_id = NEW.id
            LOOP
                IF NOT (child_birthdate << NEW.deathdate) THEN
                    RAISE integrity_constraint_violation USING MESSAGE = 'Cannot change a parent to have died before than any of its childrens birthdates';
                END IF;
            END LOOP;
        END IF;

        -- Return NEW for all non-DELETE operations.
        RETURN NEW;
    END;
$remains_alive_before_children_born$ LANGUAGE plpgsql;
