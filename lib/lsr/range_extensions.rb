# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Range # rubocop:disable Style/Documentation
  ##
  # For Ranges on Date objects, return whether the range covers a single day.
  # This may work on other types, but should only be used within LSR for
  # determining whether a birthdate is an exact day or really a range.
  def single_value?
    ((self.begin == last) && !exclude_end?) ||
      ((last == self.begin + 1) && exclude_end?)
  end
end
