# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "erector"

module LSR
  module Web
    module Icons
      ##
      # Icon for the given sex
      #
      # The optional :label_type parameter determines whether the label is
      # "male/female" or "father/mother".
      class Sex < Erector::Widget
        needs :sex, :label_type

        def label
          case @label_type.to_sym
          when :sex
            @sex.to_s.capitalize
          when :parent
            case @sex
            when :male, :father
              "Father"
            when :female, :mother
              "Mother"
            else
              raise InvalidSexError
            end
          else
            raise ArgumentError
          end
        end

        def content
          case @sex.to_sym
          when :male, :father
            span class: "fa fa-mars fa-lg", "aria-hidden": true, "aria-label": "Male"
          when :female, :mother
            span class: "fa fa-venus fa-lg", "aria-hidden": true, "aria-label": "Female"
          else
            raise InvalidSexError
          end
        end
      end
    end
  end
end
