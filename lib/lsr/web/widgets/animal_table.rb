# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/web/widgets/icons"

require "erector"

module LSR
  module Web
    ##
    # The Erector widget for rendering the animal table.
    class AnimalTable < Erector::Widget
      needs :row_objects, :edit_action
      def content
        table class: "table" do
          heading
          body
        end
      end

      private

      def heading
        thead do
          tr do
            th "Stud"
            th "Name"
            th "Birthdate"
            th "Deathdate"
            th "Sex"
            th "Breed"
            th "Horns"
            th { widget Icons::Sex.new(sex: :male, label_type: :parent) }
            th { widget Icons::Sex.new(sex: :female, label_type: :parent) }
          end
        end
      end

      def format_date_range(daterange)
        if daterange.single_value?
          daterange.begin
        else
          "#{daterange.begin}…#{daterange.last}"
        end
      end

      def birthdate(animal)
        format_date_range(animal.birthdate.to_range)
      end

      def deathdate(animal)
        format_date_range(animal.deathdate.to_range) unless animal.deathdate.nil?
      end

      def sex(animal)
        widget Icons::Sex.new(sex: animal.sex, label_type: :sex)
      end

      def breed(animal)
        animal.breed || "Unknown"
      end

      def horned_or_polled(animal)
        if animal.horned
          "Horned"
        else
          "Polled"
        end
      end

      def parent_name(animal, which_parent)
        parent = animal.__send__(which_parent)
        "#{parent.stud&.name} #{parent.name}" unless parent.nil?
      end

      def edit_button(animal)
        title = "Edit Record"
        a href: "#{@edit_action}?id=#{animal.id}", title: title,
          class: "btn btn-default btn-xs", "aria-label": title do
            span class: "fa fa-pencil fa-lg", "aria-hidden": true
          end
      end

      def body # rubocop:disable Metrics/AbcSize
        @row_objects.each do |animal|
          tr do
            # A note on blocks vs plain arguments: when the widget content is
            # just a string, it can be passed as an argument, but when it is a
            # widget (or multiple widgets), they must be within a block
            # argument.
            td animal.stud&.name
            td animal.name
            td birthdate(animal)
            td deathdate(animal)
            td { sex(animal) }
            td breed(animal)
            td horned_or_polled(animal)
            td parent_name(animal, :father)
            td parent_name(animal, :mother)
            td { edit_button(animal) }
          end
        end
      end
    end

    ##
    # The page that contains the AnimalTable.
    class AnimalTablePage < Page
      needs :row_objects, :edit_action
      def content
        super do
          div class: "container" do
            div class: "page-header" do
              h1 "Records"
            end
            title = "Add Record"
            a href: @edit_action, title: title,
              class: "btn btn-default", "aria-label": title do
              span class: "fa fa-plus", "aria-hidden": true
            end
            widget AnimalTable.new row_objects: @row_objects, edit_action: @edit_action
          end
        end
      end
    end
  end
end
