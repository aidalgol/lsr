# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Allow long lines for the resource hashes.
# rubocop:disable Metrics/LineLength

require "erector"

module LSR
  module Web
    module External
      ##
      # <link> tag that pulls in Bootstrap CSS
      class BootstrapCSS < Erector::Widget
        def content
          link rel: "stylesheet",
               href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
               integrity: "sha512-6MXa8B6uaO18Hid6blRMetEIoPqHf7Ux1tnyIQdpt9qI5OACx7C+O3IVTr98vwGnlcg0LOLa02i9Y1HpVhlfiw==",
               crossorigin: "anonymous"
        end
      end

      ##
      # <link> tag that pulls in the Bootstrap Theme
      class BootstrapTheme < Erector::Widget
        def content
          link rel: "stylesheet",
               href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css",
               integrity: "sha512-tqup5t5XyJXlvX8b/E9pI0swGA4AugszFqvdWASa2/484oj4HW7UaXKwRmnMLKFp3ZWeTwjjCh6J40AvdUQhrA==",
               crossorigin: "anonymous"
        end
      end

      ##
      # <script> tag that pulls in Bootstrap JS
      class BootstrapJS < Erector::Widget
        def content
          script src: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js",
                 integrity: "sha512-iztkobsvnjKfAtTNdHkGVjAYTrrtlC7mGp/54c40wowO7LhURYl3gVzzcEqGl/qKXQltJ2HwMrdLcNUdo+N/RQ==",
                 crossorigin: "anonymous"
        end
      end
    end
  end
end
