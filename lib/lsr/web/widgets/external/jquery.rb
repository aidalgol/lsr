# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Allow long lines for the resource hashes.
# rubocop:disable Metrics/LineLength

require "erector"

module LSR
  module Web
    module External
      ##
      # <script> tag that pulls in JQuery
      class JQuery < Erector::Widget
        def content
          script src: "https://code.jquery.com/jquery-3.1.1.min.js",
                 integrity: "sha512-U6K1YLIFUWcvuw5ucmMtT9HH4t0uz3M366qrF5y4vnyH6dgDzndlcGvH/Lz5k8NFh80SN95aJ5rqGZEdaQZ7ZQ==",
                 crossorigin: "anonymous"
        end
      end

      ##
      # <script> tag that pulls in JQuery UI CSS
      class JQueryUICSS < Erector::Widget
        def content
          link rel: "stylesheet", href: "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
               integrity: "sha512-/Q1sBqvNZheW2yvAccKiu/xc/o2AtDS2jNBozDEqA/8Mk/IcH853wrwDSGqAdl7jFyOWOcefLtwDd3kYo276Hw==",
               crossorigin: "anonymous"
        end
      end

      ##
      # <script> tag that pulls in JQueryUI
      class JQueryUI < Erector::Widget
        def content
          script src: "https://code.jquery.com/ui/1.12.1/jquery-ui.min.js",
                 integrity: "sha512-4DaiBX8rsgOoBSNLceQ/IixDF+uUDV0hJrQX/MJ9RwJZCDqbEp0EjIQodGxszPtTpwlenJznR2jkgDWqj4Hs+A==",
                 crossorigin: "anonymous"
        end
      end
    end
  end
end
