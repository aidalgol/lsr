# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/web/widgets/external/jquery"
require "lsr/web/widgets/external/bootstrap"
require "lsr/web/widgets/external/fontawesome"

require "erector"

module LSR
  module Web
    ##
    # The base widget for all pages.
    #
    # Implements content and metadata that should be the same on all pages of
    # the application.
    #
    # Either subclass this and override #content, or pass a block to Page.new.
    class Page < Erector::Widgets::Page
      def doctype
        "<!DOCTYPE html>"
      end

      def html_attributes
        {lang: "en"}
      end

      def head_content
        widget External::JQuery
        widget External::JQueryUICSS
        widget External::JQueryUI

        widget External::BootstrapCSS
        widget External::BootstrapTheme
        widget External::BootstrapJS

        widget External::FontAwesome

        title "LSR"
      end
    end
  end
end
