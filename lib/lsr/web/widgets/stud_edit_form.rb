# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/web/widgets/page"
require "lsr/db"

require "erector"

module LSR
  module Web # rubocop:disable Style/Documentation
    ##
    # The page for editing studs
    class StudEditPage < Page
      needs :action

      def head_content
        super
        script src: "/assets/stud_edit_form.js"
      end

      def content
        super do
          div class: "container" do
            widget StudEditForm.new(action: @action)
          end
        end
      end
    end

    ##
    # Form for adding and editing studs
    class StudEditForm < Erector::Widget
      def content
        form action: @action,
             method: "POST",
             id: "editForm" do
          div id: "studInputs", class: "form-group" do
            LSR::DB::Stud.to_hash(:id, :name).each do |id, name|
              input class: "form-control", type: "text", name: "id#{id}", value: name
            end
          end
          add_button
          div class: "form-group" do
            button "Commit",
                   type: "submit",
                   class: "btn btn-default"
          end
        end
      end

      def add_button
        title = "Add Stud"
        button title: title, type: "button", id: "addButton",
               class: "btn btn-default", "aria-label": title do
          span class: "fa fa-plus", "aria-hidden": true
        end
      end
    end
    private_constant :StudEditForm
  end
end
