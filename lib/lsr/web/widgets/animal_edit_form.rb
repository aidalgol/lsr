# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/web/widgets/page"
require "lsr/web/widgets/icons"
require "lsr/range_extensions"
require "lsr/db"

require "erector"

module LSR
  module Web # rubocop:disable Style/Documentation
    ##
    # The page for editing animal records
    class AnimalEditPage < Page
      needs edit_form_args: {}

      def head_content
        super
        script src: "/assets/animal_edit_form.js"
      end

      def content
        super do
          div class: "container" do
            widget AnimalEditForm.new(@edit_form_args)
          end
        end
      end
    end

    ##
    # The stud input part of the AnimalEditForm.
    class StudInput < Erector::Widget
      needs :starting_value

      def content
        div class: "form-group" do
          label "Stud", for: "studInput"
          select name: "stud", class: "form-control" do
            LSR::DB::Stud.to_hash(:id, :name).each do |id, name|
              option name, value: id, selected: @starting_value == id
            end
          end
        end
      end
    end

    ##
    # The sex input part of the AnimalEditForm.
    class SexInput < Erector::Widget
      needs :starting_value

      def content
        fieldset class: "form-group" do
          legend "Sex"
          LSR::DB::Animal::SEXES.each do |sex|
            div class: "radio" do
              label do
                input type: "radio", name: "sex", value: sex,
                      checked: (!@starting_value.nil? && (@starting_value.to_sym == sex))
                widget Icons::Sex.new(sex: sex, label_type: :sex)
              end
            end
          end
        end
      end
    end
    private_constant :SexInput

    ##
    # The input part of the AnimalEditForm for the animal's horns.
    class HornsInput < Erector::Widget
      needs :starting_value

      def content
        fieldset class: "form-group" do
          legend "Horns"
          horned_radio_button
          polled_radio_button
        end
      end

      def horned_radio_button
        div class: "radio" do
          label do
            input type: "radio", name: "horns", value: true.to_s,
                  checked: @starting_value == true
            text "Horned"
          end
        end
      end

      def polled_radio_button
        div class: "radio" do
          label do
            input type: "radio", name: "horns", value: false.to_s,
                  checked: @starting_value == false
            text "Polled"
          end
        end
      end
    end
    private_constant :HornsInput

    ##
    # The input for the animal breed in the AnimalEditForm.
    class BreedInput < Erector::Widget
      needs :starting_value

      def content
        div class: "form-group" do
          label "Breed"
          select name: "breed", class: "form-control" do
            option "Unknown", value: "nil", selected: @starting_value.nil?
            LSR::DB::Animal::BREEDS.each do |breed|
              option breed, value: breed, selected: @starting_value == breed
            end
          end
        end
      end
    end
    private_constant :BreedInput

    ##
    # The input for a parent in the AnimalEditForm.
    class ParentInput < Erector::Widget
      needs :which_parent, :parent

      def content
        div class: "form-group" do
          input id: "#{@which_parent}ID", name: @which_parent.to_s, type: "hidden",
                value: (@parent&.id)
          label for: "#{@which_parent}NameInput" do
            widget Icons::Sex.new(sex: @which_parent, label_type: :parent)
          end
          input class: "form-control",
                id: "#{@which_parent}NameInput", type: "text",
                value: (@parent&.name)
        end
      end
    end
    private_constant :ParentInput

    ##
    # Base class for the birthdate and deathdate parts of the AnimalEditForm.
    class DateInput < Erector::Widget
      needs date: nil

      def range_checkbox
        div class: "checkbox" do
          label do
            input id: "#{@which_date}RangeToggle", type: "checkbox", checked: @range
            text "Date range"
          end
        end
      end

      def start_date_input(date: nil)
        element_class = "form-group"
        element_class += " col-md-6" if @range
        div class: element_class, id: "#{@which_date}StartDateInputContainer" do
          input name: (if @range then "#{@which_date}_start_date" else @which_date.to_s end),
                type: "text", value: date, id: "#{@which_date}StartDateInput",
                hidden: true
          label "Earliest date", hidden: !@range, id: "#{@which_date}StartDateLabel",
                                 for: "#{@which_date}StartDatepicker"
          div id: "#{@which_date}StartDatepicker"
        end
      end

      def end_date_input(date: nil)
        div class: "form-group col-md-6",
            id: "#{@which_date}EndDateFieldSet", hidden: !@range do
          input name: "#{@which_date}_end_date", type: "text", value: date,
                id: "#{@which_date}EndDateInput", hidden: true, disabled: !@range
          label "Latest date", id: "#{@which_date}EndDateLabel", for: "#{@which_date}EndDatepicker"
          div id: "#{@which_date}EndDatepicker"
        end
      end

      def date_container
        div class: "container" do
          start_date_input(date: (@date&.begin))
          end_date_input(date: (@date&.last))
        end
      end
    end
    private_constant :DateInput

    ##
    # Birthdate input for the AnimalEditForm.
    class BirthdateInput < DateInput
      def content
        @which_date = :birthdate
        @range = (!@date.nil? && !@date.single_value?)

        fieldset class: "form-group" do
          legend "Birthdate"
          range_checkbox
          date_container
        end
      end
    end
    private_constant :BirthdateInput

    ##
    # Deathdate input for the AnimalEditForm.
    class DeathdateInput < DateInput
      def enabled_checkbox
        div class: "checkbox" do
          label do
            input id: "deceasedToggle", type: "checkbox", checked: !@date.nil?
            text "Deceased"
          end
        end
      end

      def content
        @which_date = :deathdate

        fieldset id: "deathdateFormGroup", class: "form-group" do
          legend "Death"
          enabled_checkbox
          span id: "deathdateInputContainer" do
            range_checkbox
            date_container
          end
        end
      end
    end
    private_constant :DeathdateInput

    ##
    # Form for creating or editing an animal record
    class AnimalEditForm < Erector::Widget
      needs :action,
            id: nil, stud_id: nil, name: "", sex: "male", horns: true, breed: nil,
            birthdate: nil, deathdate: nil, father: nil, mother: nil, error_msg: nil

      def content
        error_alert(@error_msg) if @error_msg

        form action: @action,
             method: "POST",
             id: "editForm" do

          input(name: "id", value: @id, type: "hidden") unless @id.nil?

          inputs

          button "Commit",
                 type: "submit",
                 class: "btn btn-default"
        end
      end

      def error_alert(message)
        div class: "alert alert-danger", role: "alert" do
          strong message
        end
      end

      def name_input
        div class: "form-group" do
          label "Name", for: "nameInput"
          input class: "form-control", type: "text", name: "name", id: "nameInput",
                value: @name
        end
      end

      def parent_inputs
        fieldset do
          legend "Parents"
          widget ParentInput.new(which_parent: :father, parent: @father)
          widget ParentInput.new(which_parent: :mother, parent: @mother)
        end
      end

      def inputs
        widget StudInput.new(starting_value: @stud_id)
        name_input
        widget SexInput.new(starting_value: @sex)
        widget HornsInput.new(starting_value: @horns)
        widget BreedInput.new(starting_value: @breed)
        widget BirthdateInput.new(date: @birthdate)
        widget DeathdateInput.new(date: @deathdate)
        parent_inputs
      end
    end
    private_constant :AnimalEditForm
  end
end
