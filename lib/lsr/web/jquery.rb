# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "json"

module LSR
  module Web
    ##
    # Transform internal data into formats expected by jQuery.
    module JQuery
      AUTOCOMPLETE_LIMIT = 5

      def self.jquery_autocomplete_animal_data_from_query(query)
        query.order(:name, :birthdate)
             .limit(AUTOCOMPLETE_LIMIT).collect do |animal|
          {label: animal.name, value: animal.id}
        end.to_json
      end
    end
  end
end
