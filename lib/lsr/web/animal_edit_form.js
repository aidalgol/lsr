/* This file is part of LSR, a software package for managing LiveStock Records
 * Copyright (C) 2016  Aidan and Michael Gauland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Disable the ESLint rule no-unused-vars for event handlers that are referenced
// only in HTML.  (Event handler function names should start with "on".)
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "^on" }] */

// Toggle the UI between single-date input and date-range input.
function onRangeCheckBoxChange(event, which) {
  "use strict";
  $(`#${which}StartDateInputContainer`).toggleClass("col-md-6")
  if(event.target.checked) {
    $(`#${which}EndDateFieldSet`).removeAttr("hidden")
    $(`#${which}EndDateInput`).removeAttr("disabled")
    $(`#${which}StartDateLabel`).attr("hidden", false)
    $(`#${which}StartDateInput`).attr("name", `${which}_start_date`)
  } else {
    $(`#${which}EndDateFieldSet`).attr("hidden", true)
    $(`#${which}EndDateInput`).attr("disabled", true)
    $(`#${which}StartDateLabel`).attr("hidden", true)
    $(`#${which}StartDateInput`).attr("name", `${which}`)
  }
}

// Toggle whether the form includes a deathdate.
var $deathdateInputContainer;
function onDeceasedCheckBoxChange(event) {
  "use strict";

  if(event.target.checked) {
    $("#deathdateFormGroup").append($deathdateInputContainer);
  } else {
    $deathdateInputContainer = $("#deathdateInputContainer").detach();
  }
}

function onBirthdateRangeCheckBoxChange(event) {
  "use strict";
  onRangeCheckBoxChange(event, "birthdate");
}

function onDeathdateRangeCheckBoxChange(event) {
  "use strict";
  onRangeCheckBoxChange(event, "deathdate");
}

function onSubmit() {
  "use strict";

  $("#editForm").find(":input").filter(
    function(index, element) {
      return !element.value;
    })
    .attr("disabled", "true")

  return true; // ensure form still submits
}

// Limit the end-date picker to no earlier than the start date
function onSetStartDate(dateText) {
  "use strict";
  $("#endDatepicker").datepicker("option", "minDate", dateText)
}

// We appear to need to return false for the custom event-handler to work, but
// the jQueryUI documentation makes no mention of the select event-handler's
// return value.

// Use a hidden input field for the parent IDs.
function onFatherAutocompleteSelect(_, ui) {
  "use strict";
  $("#fatherID").val(ui.item.value);
  $("#fatherNameInput").val(ui.item.label);
  return false;
}
function onMotherAutocompleteSelect(_, ui) {
  "use strict";
  $("#motherID").val(ui.item.value);
  $("#motherNameInput").val(ui.item.label);
  return false;
}

// Clear the parent-fieldset's ID field when the name field is changed.
function clearFatherID() {
  "use strict";
  $("#fatherID").val("");
}
function clearMotherID() {
  "use strict";
  $("#motherID").val("");
}

// Clear the parent inputs when a partially-entered name is left in the field.
function onFatherNameChange(event, ui) {
  "use strict";
  if (!ui.item) {
    $("#fatherNameInput").val("");
    clearFatherID(event, ui);
  }
}
function onMotherNameChange(event, ui) {
  "use strict";
  if (!ui.item) {
    $("#motherNameInput").val("");
    clearMotherID(event, ui);
  }
}

$(function() {
  "use strict";

  // Set form-submision callback.
  $("#editForm").submit(onSubmit);

  for (let which of ["birthdate", "deathdate"]) {
    // Set up datepickers
    $(`#${which}StartDatepicker`).datepicker({
      altField: `#${which}StartDateInput`,
      dateFormat: "yy-mm-dd", // Use same format as the default Date string-format in Ruby
      defaultDate: $(`#${which}StartDateInput`).val(),
      todayHighlight: true, // Necessary because setting defaultDate disables this
      changeMonth: true,
      changeYear: true
    });
    $(`#${which}EndDatepicker`).datepicker({
      altField: `#${which}EndDateInput`,
      dateFormat: "yy-mm-dd", // Use same format as the default Date string-format in Ruby
      defaultDate: $(`#${which}EndDateInput`).val(),
      todayHighlight: true, // Necessary because setting defaultDate disables this
      changeMonth: true,
      changeYear: true
    });
  }

  // Bind checkbox events.
  $("#birthdateRangeToggle").on("click", onBirthdateRangeCheckBoxChange);
  $("#deathdateRangeToggle").on("click", onDeathdateRangeCheckBoxChange);
  $("#deceasedToggle").on("click", onDeceasedCheckBoxChange);

  // Set up parent input fields.
  // Father input field.
  $("#fatherNameInput").autocomplete({
    source: "/jquery-autocomplete/animal?sex=male",
    select: onFatherAutocompleteSelect,
    search: clearFatherID,
    change: onFatherAutocompleteSelect,
    minLength: 2,
  });

  // Mother input field.
  $("#motherNameInput").autocomplete({
    source: "/jquery-autocomplete/animal?sex=female",
    select: onMotherAutocompleteSelect,
    change: onMotherNameChange,
    search: clearMotherID,
    minLength: 2,
  });
});
