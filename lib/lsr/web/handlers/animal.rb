# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/db"
require "lsr/range_extensions"
require "lsr/coercion"
require "lsr/web/exceptions"
require "lsr/web/widgets/animal_edit_form"

module LSR
  module Web
    ##
    # Request handlers for the Sinatra app.
    module Handlers
      def self.animal_table(edit_action)
        LSR::Web::AnimalTablePage.new(
          edit_action: edit_action,
          row_objects: LSR::DB::Animal.dataset.order(:birthdate).all).to_html
      end

      def self.animal_edit_form(action, params) # rubocop:disable Metrics/AbcSize
        if params.key? "id"
          animal = LSR::DB::Animal[id: params["id"]]
          LSR::Web::AnimalEditPage.new(
            edit_form_args: {
              action: action, id: animal.id, stud_id: animal.stud_id, name: animal.name,
              sex: animal.sex, horns: animal.horned, breed: animal.breed,
              birthdate: animal.birthdate.to_range, deathdate: animal.deathdate&.to_range,
              father: animal.father, mother: animal.mother
            })
        else
          LSR::Web::AnimalEditPage.new(edit_form_args: {action: action})
        end.to_html
      end

      def self.edit_form_extract_birthdate(post_params)
        if post_params.key? "birthdate"
          # Birthdate is an exact day.
          (Date.parse(post_params["birthdate"])..
           Date.parse(post_params["birthdate"]))
        elsif post_params.key?("birthdate_start_date") && post_params.key?("birthdate_end_date")
          # Birthdate is a range.
          (Date.parse(post_params["birthdate_start_date"])..
           Date.parse(post_params["birthdate_end_date"]))
        else
          # Invalid POST birthdate parameters
          raise LSR::Web::POSTDataError, "Invalid birthdate parameters"
        end
      end
      private_class_method :edit_form_extract_birthdate

      def self.edit_form_extract_deathdate(post_params)
        if post_params.key? "deathdate"
          # Deathdate is an exact day.
          (Date.parse(post_params["deathdate"])..
           Date.parse(post_params["deathdate"]))
        elsif post_params.key?("deathdate_start_date") && post_params.key?("deathdate_end_date")
          # Deathdate is a range.
          (Date.parse(post_params["deathdate_start_date"])..
           Date.parse(post_params["deathdate_end_date"]))
        elsif post_params.key?("deathdate_start_date") || post_params.key?("deathdate_end_date")
          # It is an error to have only a start or end date.
          raise LSR::Web::POSTDataError, "Invalid deathdate parameters"
        end
      end
      private_class_method :edit_form_extract_deathdate

      def self.extract_edit_form_params_for_db(params) # rubocop:disable Metrics/AbcSize
        {name: params["name"],
         stud_id: params["stud"],
         sex: params["sex"],
         horned: params["horns"],
         breed: LSR::Coercion.nil_or_string(params["breed"]),
         birthdate: edit_form_extract_birthdate(params).pg_range,
         deathdate: edit_form_extract_deathdate(params)&.pg_range,
         father_id: (params["father"] && LSR::Coercion.string_to_integer_or_nil(params["father"])),
         mother_id: (params["mother"] && LSR::Coercion.string_to_integer_or_nil(params["mother"]))}
      end
      private_class_method :extract_edit_form_params_for_db

      def self.animal_update(params)
        LSR::DB::Animal[id: params["id"]].update(extract_edit_form_params_for_db(params))
      end

      def self.animal_create(params)
        LSR::DB::Animal.create(extract_edit_form_params_for_db(params))
      end

      def self.extract_edit_form_params_for_edit_form_args(params) # rubocop:disable Metrics/AbcSize
        {id: params["id"],
         name: params["name"],
         stud_id: params["stud"],
         sex: params["sex"],
         horns: params["horns"] == "true",
         breed: LSR::Coercion.nil_or_string(params["breed"]),
         birthdate: edit_form_extract_birthdate(params),
         deathdate: edit_form_extract_deathdate(params),
         father: (params[:father] &&
                  LSR::DB::Animal[LSR::Coercion.string_to_integer_or_nil(params["father"])]),
         mother: (params[:mother] &&
                  LSR::DB::Animal[LSR::Coercion.string_to_integer_or_nil(params["mother"])])}
      end
      private_class_method :extract_edit_form_params_for_edit_form_args

      def self.on_edit_page_error(action, post_params, exception)
        args = {action: action,
                error_msg: exception.message}
               .merge(extract_edit_form_params_for_edit_form_args(post_params))
        LSR::Web::AnimalEditPage.new(edit_form_args: args).to_html
      end

      def self.jquery_autocomplete_animal_query(params)
        raise LSR::Web::InvalidGETParams, "No search term given" unless params.key? "term"
        raise LSR::Web::InvalidGETParams, "No sex specified" unless params.key? "sex"

        unless LSR::DB::Animal::SEXES.include?(params["sex"].to_sym)
          raise LSR::Web::InvalidGETParams, "Invalid sex specified: \"#{params['sex']}\""
        end

        LSR::DB::Animal.where(Sequel.ilike(:name, "#{params['term']}%"),
                              sex: params["sex"])
      end
    end
  end
end
