# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "lsr/db"
require "lsr/web/widgets/stud_edit_form"

module LSR
  module Web
    ##
    # Request handlers for the Sinatra app.
    module Handlers
      def self.studs_edit_form(action)
        LSR::Web::StudEditPage.new(action: action).to_html
      end

      def self.studs_update(params)
        # Create new stud items from the form.
        new_stud_names = params.select { |key| key[0..2] == "new" }.values
        new_stud_names.each do |name|
          LSR::DB::Stud.create(name: name)
        end

        # Update existing stud items.
        existing_studs = params.select { |key| key[0..1] == "id" }
        existing_studs.each do |key, name|
          id = key[2..-1]
          stud = LSR::DB::Stud[id: Integer(id)]
          stud.update(name: name)
        end
      end
    end
  end
end
