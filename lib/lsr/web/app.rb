# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "sequel"
Sequel.extension :core_extensions
Sequel.extension :pg_range
# Using ||= so that we don't create a new DB connection on Sinatra reloads.
DB ||= Sequel.connect(ENV.fetch("DATABASE_URL"))

require "lsr/db"
require "lsr/web/widgets"
require "lsr/web/handlers/animal"
require "lsr/web/handlers/studs"
require "lsr/web/jquery"
require "lsr/web/exceptions"

require "sinatra/base"

module LSR
  module Web
    ##
    # The Sinatra application for LSR's web UI.
    class App < Sinatra::Base
      configure do
        enable :logging
      end

      configure :development do
        require "sinatra/reloader"
        register Sinatra::Reloader
        Dir.glob(File.join(File.dirname(__FILE__), "**/*.rb")).each do |path|
          also_reload path
        end
      end

      configure :development, :test do
        require "erector"
        Erector::AbstractWidget.prettyprint_default = true
      end

      get "/animals" do
        Handlers.animal_table("/animals/edit")
      end

      get "/animals/edit" do
        Handlers.animal_edit_form("/animals/edit", params)
      end

      post "/animals/edit" do
        begin
          if params.key? "id"
            Handlers.animal_update(params)
          else
            Handlers.animal_create(params)
          end
          redirect "/animals"
        rescue Sequel::DatabaseError, POSTDataError => ex
          [400, Handlers.on_edit_page_error("/animals/edit?id=#{params['id']}", params, ex)]
        end
      end

      get "/jquery-autocomplete/animal" do
        begin
          JQuery.jquery_autocomplete_animal_data_from_query(
            Handlers.jquery_autocomplete_animal_query(params))
        rescue InvalidGETParams
          400
        end
      end

      get "/studs" do
        Handlers.studs_edit_form("/studs")
      end

      post "/studs" do
        Handlers.studs_update(params)
        redirect "/studs"
      end
    end
  end
end
