# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require "sequel"
require "date"

module LSR
  module DB
    ##
    # Sequel model for studs
    class Stud < Sequel::Model
    end

    ##
    # Sequel model for animals
    class Animal < Sequel::Model
      # These must match the possible values of the sex enum type in the database.
      SEXES = [:male, :female].freeze
      BREEDS = ["Toggenburg", "Saanen", "British Alpine", "Sable", "Nubian", "Boer",
                "Angora", "Arapawa", "Rawhiti", "Kiko", "Waipu"].freeze

      many_to_one :stud, class: Stud

      many_to_one :mother, class: self
      many_to_one :father, class: self
    end
  end
end
