# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module LSR
  ##
  # Specialised type-coercion methods.
  module Coercion
    ##
    # Convert a string representation of a decimal to a number, returning nil
    # for the empty string instead of zero.
    def self.string_to_integer_or_nil(value)
      if value.respond_to?(:empty?) && value.empty?
        nil
      else
        Integer(value, 10)
      end
    end

    ##
    # Convert a string to nil if the string is "nil", otherwise return the
    # original string.
    def self.nil_or_string(string)
      if string == "nil"
        nil
      else
        string
      end
    end
  end
end
