# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ruby:2.3
ENV appdir /usr/local/src/lsr/
ENV web_port 9292
ENV DATABASE_URL postgres://postgres@db:5432/postgres
EXPOSE ${web_port}

RUN mkdir -p ${appdir}
COPY Gemfile ${appdir}
WORKDIR ${appdir}
RUN bundle install --without testing development
COPY config.ru ${appdir}
COPY lib ${appdir}/lib

# We're done with tasks that require root access, so switch to an unprivileged
# user.
RUN groupadd -r lsr && useradd -r -g lsr lsr
USER lsr

CMD bundler exec rackup -I lib --host 0.0.0.0 -p ${web_port}
