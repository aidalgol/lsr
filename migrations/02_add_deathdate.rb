# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

Sequel.migration do
  change do
    extension :pg_range

    alter_table(:animals) do
      add_column :deathdate, :daterange
    end

    # Load trigger procedures into the database.
    Dir.chdir("triggers/02") do
      run(File.read("child_born_before_parents_death.sql"))
      run(File.read("remains_alive_before_children_born.sql"))
    end

    create_trigger(:animals, :child_born_before_parents_death, :child_born_before_parents_death,
                   each_row: true)
    create_trigger(:animals, :remains_alive_before_children_born,
                   :remains_alive_before_children_born, each_row: true)
  end
end
