# frozen_string_literal: true

# This file is part of LSR, a software package for managing LiveStock Records
# Copyright (C) 2016  Aidan and Michael Gauland
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

Sequel.migration do
  change do
    extension :pg_enum
    extension :pg_range

    create_enum(:sex, %w(male female))
    create_enum(:breed, ["Toggenburg", "Saanen", "British Alpine", "Sable", "Nubian", "Boer",
                         "Angora", "Arapawa", "Rawhiti", "Kiko", "Waipu"])

    create_table(:studs) do
      primary_key :id
      String      :name, null: false
    end

    create_table(:animals) do
      primary_key :id
      String      :name
      daterange   :birthdate, null: false
      sex         :sex, null: false
      foreign_key :father_id, :animals, on_delete: :restrict
      foreign_key :mother_id, :animals, on_delete: :restrict
      TrueClass   :horned, null: false, default: true
      breed       :breed
      foreign_key :stud_id, :studs, on_delete: :restrict

      constraint(:birthdate_nonempty_range) { ~isempty(birthdate) }
    end

    # Load trigger procedures into the database.
    Dir.chdir("triggers/01") do
      run(File.read("not_own_parent.sql"))
      run(File.read("correct_parent_sexes.sql"))
      run(File.read("parents_born_before_child.sql"))
      run(File.read("no_sex_change_with_children.sql"))
      run(File.read("remains_older_than_children.sql"))
      run(File.read("breed_and_parents_mutually_exclusive.sql"))
    end

    create_trigger(:animals, :not_own_parent, :not_own_parent, each_row: true)
    create_trigger(:animals, :correct_parent_sexes, :correct_parent_sexes, each_row: true)
    create_trigger(:animals, :parents_born_before_child, :parents_born_before_child, each_row: true)
    create_trigger(:animals, :no_sex_change_with_children, :no_sex_change_with_children,
                   each_row: true)
    create_trigger(:animals, :remains_older_than_children, :remains_older_than_children,
                   each_row: true)
    create_trigger(:animals, :breed_and_parents_mutually_exclusive,
                   :breed_and_parents_mutually_exclusive, each_row: true)
  end
end
